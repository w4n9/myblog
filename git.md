##oscgit使用
@[git|oscgit|Markdown]

####向服务器提交代码时的用户名邮箱
``` shell
git config --global user.name "Your Name"  
git config --global user.email "your@email.com"
```

####创建公钥
``` shell
ssh-keygen -t rsa -C "youremail@xxx.com"
```

####测试连接是否畅通
``` shell
ssh -T git@git.oschina.net
Welcome to Git@OSC, whaon!
```

####以https的方式添加git
```shell
git remote add origin https://git.oschina.net/whaon/GitHelloWolrd.git
[remote "origin"]
	url = http://git.oschina.net/whaon/none.git
	fetch = +refs/heads/*:refs/remotes/origin/*
	
git remote rm origin
git remote add origin http://git.oschina.net/whaon/GitHelloWolrd.git
```

####提交与更新
``` shell
git init  #初始化
git add .  #把该目录下的所有文件全部提交到缓冲区
git commit -m "GitHelloWorld first commit"  #将代码提交到HEAD，注意此时还没有提交到服务器
git pull origin master  #更新代码
git push origin master  #提交代码
```

####git警告换行方式不统一
> git config --global core.autocrlf false