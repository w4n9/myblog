##Mysql

###MySQL中的数据类型
####数字类型
> 整数: tinyint、smallint、mediumint、int、bigint
> 
> 浮点数: float、double、real、decimal

####连接MySQL
> mysql -h 远程主机地址 -u 用户名 -p

####数据库简单操作
######显示数据库列表
> \>show databases; 